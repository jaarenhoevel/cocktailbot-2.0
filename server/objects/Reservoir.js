class Reservoir {
	constructor(id, size, content, amount, valveId) {
		this.id = id;
		this.size = size;
		this.content = content;
		this.amount = amount;
		this.valveId = valveId;
	}
	
	toSerialObject() {
		return {
			id: this.id,
			size: this.size,
			content: this.content.id,
			amount: this.amount,
			valveId: this.valveId
		};
	}
}

module.exports = Reservoir;