class Ingredient {
	constructor(id, name, percentage) {
		this.id = id;
		this.name = name;
		this.percentage = percentage;
	}
	
	toSerialObject() {
		return this;
	}
}

module.exports = Ingredient;