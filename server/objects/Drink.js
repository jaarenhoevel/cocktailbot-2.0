class Drink {
	constructor(id, name, color) {
		this.id = id;
		this.name = name;
		this.ingredients = [];
		this.color = color;
	}
	
	addIngredient(ingredient, amount) {
		this.ingredients.push([ingredient, amount]);
	}
	
	toSerialObject() {
		var tempIngredients = [];
		this.ingredients.forEach((i) => {
			tempIngredients.push([i[0].id, i[1]]);
		});
		
		return {
			id: this.id,
			name: this.name,
			ingredients: tempIngredients,
			color: this.color
		};
	}
}

module.exports = Drink;