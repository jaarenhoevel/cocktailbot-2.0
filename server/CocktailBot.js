const fs = require("fs");

const Machine = require('./Machine.js');

const Drink = require('./objects/Drink.js');
const Ingredient = require('./objects/Ingredient.js');
const Reservoir = require('./objects/Reservoir.js');

class CocktailBot {
	constructor(dryRun) {
		this.machine = new Machine(dryRun);
			
		this.ingredients = [];
		this.drinks = [];
		this.reservoirs = [];
		this.state = {
			status: "idle",
			drink: null,
			ingredientIndex: 0,
			progress: 0
		};
		this.onStateChange = () => {};
		this.onError = () => {};
		
		var tempIngredients = JSON.parse(fs.readFileSync("./database/Ingredients.json"));
		var tempDrinks = JSON.parse(fs.readFileSync("./database/Drinks.json"));
		var tempReservoirs = JSON.parse(fs.readFileSync("./database/Reservoirs.json"));
		
		
		tempIngredients.forEach((i) => {
			this.ingredients.push(new Ingredient(i.id, i.name, i.percentage));
		});
		
		tempDrinks.forEach((d) => {
			var drink = new Drink(d.id, d.name, d.color);
			d.ingredients.forEach((i) => {
				drink.addIngredient(CocktailBot.getIngredientById(this.ingredients, i[0]), i[1]);
			});
			this.drinks.push(drink);
		});
		
		tempReservoirs.forEach((r) => {
			this.reservoirs.push(new Reservoir(r.id, r.size, CocktailBot.getIngredientById(this.ingredients, r.content), r.amount, r.valveId));
		});
	}
	
	/**
	 * Makes specified amount of a drink
	 * @param {Drink} drink
	 * @param {Number} amount 
	 * @param {Boolean} ignoreMissingIngredients
	 * @returns {Boolean} started successfully  
	 */
	makeDrink(drink, amount, ignoreMissingIngredients = false) {			
		if (this.state.status !== "idle") {
			this.onError({
				type: "error",
				text: "Maschine ist beschäftigt."
			});
			return false;
		}
		
		if (!drink.hasOwnProperty("ingredients")) return false;
			
		if (!ignoreMissingIngredients && !this.checkIfDrinkIsAvailable(drink, amount)) {
			this.onError({
				type: "error",
				text: "Nicht genügend Zutaten vorhanden."
			});
			return false;
		}
		
		this.state.drink = drink;
		this.state.status = "filling";
		this.state.ingredientIndex = 0;
		this.onStateChange();
		
		var resetState = () => {
			this.state.drink = null;
			this.state.status = "idle";
			this.state.ingredientIndex = 0;
			this.state.progress = 0;
			this.onStateChange();
		};
		
		// Recursive function to fill ingredients	
		var recursify = (ingredients, amount) => {
			if (ingredients.length === 0) { // no more ingredients -> completed
				resetState();
				this.onError({
					type: "success",
					text: "Drink ist fertiggestellt."
				});
				return;
			}
			
			var reservoirs = this.getIngredientReservoirs(ingredients[0][0]);
			
			if (reservoirs.length > 0) {
				var fillingStarted = this.machine.fill(reservoirs[0].valveId, ingredients[0][1] * amount, (p) => {
					// status callback with current filling progress
					this.state.progress = p;
					this.onStateChange();
				}, () => { // error filling ingredient
					reservoirs[0].amount = 0;
					this.onError({
						type: "info",
						text: ingredients[0][0].name + " konnte nicht aus Reservoir #" + reservoirs[0].id + " eingefüllt werden."
					});
					recursify(ingredients, amount);
				}, () => { // filling of ingredient succeeded
					reservoirs[0].amount -= ingredients[0][1] * amount;
					
					if (ingredients.length > 1) {
						this.state.ingredientIndex ++;
						this.state.progress = 0;
						this.onStateChange();
					}
					ingredients.shift();
					
					recursify(ingredients, amount);
				});
				
				if (!fillingStarted) { // machine refused to start filling progress
					this.onError({
						type: "error",
						text: "Die Befüllung konnte nicht gestartet werden."
					});
					resetState();
				}
			} else if (!ignoreMissingIngredients) { // no reservoirs with ingredient
				this.onError({
					type: "error",
					text: ingredients[0][0].name + " ist in keinem Reservoir verfügbar."
				});
				resetState();
			} else {
				if (ingredients.length > 1) {
					this.state.ingredientIndex ++;
					this.state.progress = 0;
					this.onStateChange();
				}
				ingredients.shift();
					
				recursify(ingredients, amount);
			}
		};
		
		recursify(drink.ingredients.slice(), amount);
		return true;
	}
	
	/**
	 * Checks if a drink is available to make
	 * @param {Drink} drink
	 * @param {Number} amount in ml
	 * @param {Boolean} missingIngredients return list of missing ingredients 
	 * @returns {Boolean / Array} is available / missing ingredients
	 */
	checkIfDrinkIsAvailable(drink, amount, missingIngredients = false) {
		var available = true;
		var missing = [];
		
		drink.ingredients.forEach((drinkIngredient) => {
			var combinedAmount = this.getCombinedIngredientAmount(drinkIngredient[0]);
			
			if (combinedAmount < drinkIngredient[1] * amount) {
				available = false;
				missing.push([drinkIngredient[0], (drinkIngredient[1] * amount) - combinedAmount]);
			}
		});
		
		return missingIngredients ? missing : available;
	}
	
	/**
	 * Calculates cumulutive ingredient amount
	 * @param {Ingredient} ingredient
	 * @returns {Number}
	 */
	getCombinedIngredientAmount(ingredient) {
		var reservoirs = this.getIngredientReservoirs(ingredient);
		var amount = 0;
		
		reservoirs.forEach((reservoir) => {
			amount += reservoir.amount;
		});
		
		return amount;
	}
	
	/**
	 * Returns array of all reservoirs containing given ingredient
	 * @param {Ingredient} ingredient
	 * @returns {Array} Reservoirs
	 */
	getIngredientReservoirs(ingredient) {
		var reservoirs = [];
		this.reservoirs.forEach((reservoir) => {
			if (reservoir.content.id === ingredient.id && reservoir.amount > 0) reservoirs.push(reservoir);
		});
		
		return reservoirs;
	}
	
	/**
	 * Saves current state of CocktailBot to database
	 */
	save() {
		var tempIngredients = [];
		var tempDrinks = [];
		var tempReservoirs = [];
		
		this.ingredients.forEach((i) => {
			tempIngredients.push(i.toSerialObject());
		});
		
		this.drinks.forEach((d) => {
			tempDrinks.push(d.toSerialObject());
		});
		
		this.reservoirs.forEach((r) => {
			tempReservoirs.push(r.toSerialObject());
		});
		
		fs.writeFile("./database/Ingredients.json", JSON.stringify(tempIngredients));
		fs.writeFile("./database/Drinks.json", JSON.stringify(tempDrinks));
		fs.writeFile("./database/Reservoirs.json", JSON.stringify(tempReservoirs));
	}
	
	/**
	* Returns ingredient with given id
	* @param {Array} ingredients
	* @param {String} id
	* @returns {Ingredient}
	*/
    static getIngredientById(ingredients, id) {
	   for (let i = 0; i < ingredients.length; i ++) {
		   if (ingredients[i].id === id) return ingredients[i];
	   } 
	   return null;
   }
}

module.exports = CocktailBot;