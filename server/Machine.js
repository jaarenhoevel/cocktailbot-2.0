class Machine {
	constructor(dryRun) {
		this.dryRun = dryRun;
		this.weight = 0;
		
		this.fillObserver = null;
		
		if (!dryRun) {
			const SerialPort = require("serialport");
			const SerialParser = require("@serialport/parser-readline");
			
			this.port = new SerialPort("/dev/ttyUSB0", {
				baudRate: 115200
			});
			
			const parser = new SerialParser();
			this.port.pipe(parser);
			
			parser.on("data", (line) => {
				var data = line.split(" ");
				
				switch(data[0]) {
					case "SCALE":
						this.weight = +(data[1]);
						break;
					default:
						break;
				}
			});
			
			setInterval(() => {
				this.port.write("SCALE\r\n");
			}, 500);
		}
	}
	
	/**
	 * Opens given valve and pumps until given amount is reached
	 * @param {String} valveId
	 * @param {Number} amount
	 * @param {function} callback
	 * @param {function} error
	 * @param {function} success  
	 * @returns {Boolean} started 
	 */
	fill(valveId, amount, callback, error, success) {
		if (this.fillObserver !== null) {
			return false;
		}
		
		this.openValve(valveId);
		this.startPump(true);
	
		this.fillObserver = {
			interval: setInterval(() => {
				if (this.dryRun) this.weight += 5;
				
				this.fillObserver.elapsed ++;
				
				if (this.weight > this.fillObserver.lastWeight) {
					this.fillObserver.lastWeight = this.weight;
					
					var progress = (this.weight - this.fillObserver.startWeight) / amount;
						
					callback((progress > 1) ? 1 : parseFloat(progress.toFixed(2)));
					this.fillObserver.elapsed = 0;
					
					if (progress > 1) {
						this.stopPump();
						this.startPump(false);
						
						setTimeout(() => {
							this.closeValve(valveId);
							this.stopPump();
							success();
						}, 2500);
						
						clearInterval(this.fillObserver.interval);
						this.fillObserver = null;
						return;
					}
				}
				
				if (this.fillObserver.elapsed > 10) {
					this.closeValve(valveId);
					this.stopPump();
					
					clearInterval(this.fillObserver.interval);
					this.fillObserver = null;
					error();
				}
				
			}, 500),
			elapsed: 0,
			startWeight: this.weight,
			lastWeight: this.weight
		};
		
		return true;
	}
	
	/**
	 * Opens valve
	 * @param {String} valveId 
	 */
	openValve(valveId) {
		if (this.dryRun) {
			console.log("Opened valve", valveId);
			return;
		}
		
		this.port.write("VALVE " + valveId + " OPEN\r\n");
	}
	
	/**
	 * Closes valve
	 * @param {String} valveId 
	 */
	closeValve(valveId) {
		if (this.dryRun) {
			console.log("Closed valve", valveId);
			return;
		}
		
		this.port.write("VALVE " + valveId + " CLOSE\r\n");
	}
	
	/**
	 * Starts pumping
	 * @param {Boolean} forwards pumping direction 
	 */
	startPump(forwards) {
		if (this.dryRun) {
			console.log("Started pumping", (forwards) ? "forwards" : "backwards");
			return;
		}
		
		if (forwards) {
			this.port.write("PUMP FORWARDS\r\n");
		} else {
			this.port.write("PUMP BACKWARDS\r\n");
		}
	}
	
	/**
	 * Stops pump
	 */
	stopPump() {
		if (this.dryRun) {
			console.log("Stopped pump");
			return;
		}
		
		this.port.write("PUMP STOP\r\n");
	}
}

module.exports = Machine;