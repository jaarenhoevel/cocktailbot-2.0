const port = 3000;

const express = require('express');
const app = express();
const http = require('http');
const server = http.createServer(app);
const {Server} = require("socket.io");
const io = new Server(server);

app.use(express.static('webapp'));
app.use('*', (req, res) => {
    res.sendFile(__dirname + '/webapp/index.html');
});


process.stdin.resume(); // program will not close instantly

[`exit`, `SIGINT`, `SIGUSR1`, `SIGUSR2`, `uncaughtException`, `SIGTERM`].forEach((eventType) => {
  	process.on(eventType, () => {
	  	bot.save();
	  	process.exit();
  	});
});

const CocktailBot = require('./CocktailBot.js');
const bot = new CocktailBot(true);

bot.onError = (e) => {
	io.emit("error", e);
};

bot.onStateChange = () => {
	io.emit("state", bot.state);
};

io.on("connection", (socket) => {
	console.log('a display connected');
	socket.on('disconnect', () => {
		console.log('display disconnected');
	});
	
	socket.on("getDrinks", () => {
		socket.emit("drinks", bot.drinks);
	});
	
	socket.on("getReservoirs", () => {
		socket.emit("reservoirs", bot.reservoirs);
	});
	
	socket.on("getIngredients", () => {
		socket.emit("ingredients", bot.ingredients);
	});
	
	socket.on("getState", () => {
		socket.emit("state", bot.state);
	});
	
	socket.on("makeDrink", (d, a) => bot.makeDrink(d, a));
});

server.listen(port);
