export default class Interface {
		constructor() {
			this.backdropDOM = document.getElementById("backdrop");
			
			this.stateBarDOM = document.getElementById("bar");
			this.stateBarCoverDOM = document.getElementById("cover");
			this.stateTextDOM = document.getElementById("stateText");
			this.stateBarColorOverlayDOM = document.getElementById("colorOverlay");
			
			this.drinkDOM = document.getElementById("drinks");
			
			this.messageDOM = document.getElementById("message");
			
			this.makeDrink = () => {};
			
			this.orderWindowDOM = document.getElementById("order");
			this.orderGoDOM = document.getElementById("go");
			
			this.orderIncreaseDOM = document.getElementById("orderIncrease");
			this.orderDecreaseDOM = document.getElementById("orderDecrease");
			this.orderAmountDOM = document.getElementById("orderAmount");
			
			this.orderAmount = 250;
			this.updateOrderAmount();
			
			this.orderIncreaseDOM.onclick = () => {
				this.orderAmount += 25;
				this.updateOrderAmount();
			}
			
			this.orderDecreaseDOM.onclick = () => {
				if (this.orderAmount < 50) return;
				
				this.orderAmount -= 25;
				this.updateOrderAmount();
			}
		}
		
		/**
		 * Draws state bar
		 * @param {Object} state 
		 */
		drawState(state) {
			console.log(state);
			
			if (state.status === "idle") {
				this.stateTextDOM.innerHTML = "Bereit";
				this.stateBarDOM.innerHTML = "";
				this.stateBarCoverDOM.style.width = "110%";
				this.stateBarColorOverlayDOM.style.backgroundColor = "#fff";
				return;
			}
			
			this.stateTextDOM.innerHTML = "Zutat " + (state.ingredientIndex + 1) + "/" + state.drink.ingredients.length + ": " + state.drink.ingredients[state.ingredientIndex][0].name;
			
			if (this.stateBarDOM.dataset.drink !== state.drink.id) {
				this.stateBarColorOverlayDOM.style.backgroundColor = state.drink.color;
				
				this.stateBarDOM.innerHTML = "";
				
				state.drink.ingredients.forEach((i) => {
					var piece = document.createElement("div");
					piece.classList.add("bar");
					piece.style.width = String(i[1] * 100) + "%";
					this.stateBarDOM.appendChild(piece);
				});
				
				this.stateBarDOM.dataset.drink = state.drink.id;
			}
			
			var globalProgress = state.progress * state.drink.ingredients[state.ingredientIndex][1];
			
			if (state.ingredientIndex > 0) {
				for (var i = state.ingredientIndex - 1; i >= 0; i --) {
					globalProgress += state.drink.ingredients[i][1];
				}
			}
			
			this.stateBarCoverDOM.style.width = String((1 - globalProgress) * 100) + "%";
		}
		
		/**
		 * Displays message for a short time
		 * @param {String} message
		 * @param {String} type
		 * @param {Number} duration   
		 */
		showMessage(message, type = "info", duration = 2500) {
			this.messageDOM.innerHTML = message;
			switch(type) {
				case "error":
					this.messageDOM.style.backgroundColor = "#933";
					break;
				case "success":
					this.messageDOM.style.backgroundColor = "#294";
					break;
				default:
					this.messageDOM.style.backgroundColor = "#36f";
					break;
			}
			
			this.messageDOM.style.bottom = "75px";
			
			setTimeout(() => {
				this.messageDOM.style.bottom = "-200px";
			}, duration);
		}
		
		/**
		 * Draws drinks
		 * @param {Array} drinks 
		 */
		drawDrinks(drinks) {
			this.drinkDOM.innerHTML = "";
			
			drinks.forEach((d) => {
				this.addDrink(d);
			});
			
			var clearFix = document.createElement("div");
			clearFix.classList.add("clearfix");
			
			this.drinkDOM.appendChild(clearFix);
		}
		
		/**
		 * Adds drink to drink list
		 * @param {Drink} drink 
		 */
		addDrink(drink) {
			var d = document.createElement("div");
			d.classList.add("drink");
			d.style.backgroundColor = drink.color;
			
			var dName = document.createElement("p");
			dName.classList.add("drinkName");
			dName.innerHTML = drink.name;
			d.appendChild(dName);
			
			var ingredients = [];
			drink.ingredients.forEach((i) => {
				ingredients.push(i[0].name);
			});
			
			var dIngredients = document.createElement("p");
			dIngredients.classList.add("drinkIngredients");
			dIngredients.innerHTML = ingredients.join(", ");
			d.appendChild(dIngredients);
			
			var dOrder = document.createElement("p");
			dOrder.classList.add("drinkOrder");
			dOrder.innerHTML = "Zubereiten";
			
			var dOrderArrow = document.createElement("span");
			dOrderArrow.classList.add("material-icons");
			dOrderArrow.innerHTML = "keyboard_arrow_right";
			dOrder.onclick = () => {
				this.openOrderWindow(drink);
			};
			
			dOrder.appendChild(dOrderArrow);
			d.appendChild(dOrder);
			
			this.drinkDOM.appendChild(d);
		}
		
		openOrderWindow(drink) {
			this.orderGoDOM.onclick = () => {
				this.makeDrink(drink, this.orderAmount);
				this.closeOrderWindow();
			};
			
			this.orderWindowDOM.style.display = "block";
			this.showBackdrop();
		}
		
		/**
		 * closes order window
		 */
		closeOrderWindow() {
			setTimeout(() => {this.orderWindowDOM.style.display = "none"; console.log(this)}, 250);
			this.hideBackdrop();
		}
		
		/**
		 * Updates amount in order window
		 */
		updateOrderAmount() {
			this.orderAmountDOM.innerHTML = this.orderAmount + " ml";
		}
		
		/**
		 * Shows backdrop
		 */
		showBackdrop() {
			this.backdropDOM.style.display = "block";
			setTimeout(() => {this.backdropDOM.style.opacity = 1;}, 25);
		}
		
		/**
		 * Hides backdrop
		 */
		hideBackdrop() {
			this.backdropDOM.style.opacity = 0;
			setTimeout(() => {this.backdropDOM.style.display = "none";}, 250);
		}
}