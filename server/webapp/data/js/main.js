import Interface from './Interface.js';

const socket = io();
const gui = new Interface();

gui.makeDrink = makeDrink;

var drinks = [];
var ingredients = [];
var reservoirs = [];

var state = {};

socket.on("drinks", (d) => {
	drinks = d;
	gui.drawDrinks(drinks);
});

socket.on("ingredients", (i) => ingredients = i);
socket.on("reservoirs", (r) => reservoirs = r);

socket.on("state", (s) => {
	state = s;
	gui.drawState(state);
});

socket.on("error", (e) => {
	gui.showMessage(e.text, e.type);
});

socket.on("connect", getData);

function getData() {
	socket.emit("getState");
	socket.emit("getDrinks");
	socket.emit("getIngredients");
	socket.emit("getReservoirs");
}

function makeDrink(d, a) {
	socket.emit("makeDrink", d, a);
}