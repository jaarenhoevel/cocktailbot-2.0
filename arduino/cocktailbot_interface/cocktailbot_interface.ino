#include "SerialCommands.h"
#include "HX711.h"
#include "Wire.h"
#include "Adafruit_MCP23X17.h"

#define BAUDRATE 115200

#define LOADCELL_DATA_PIN 2
#define LOADCELL_CLOCK_PIN 3

HX711 scale;

Adafruit_MCP23X17 expander[2];

#define PUMP_FORWARDS_EXT_PIN 1
#define PUMP_BACKWARDS_EXT_PIN 2

#define RESERVED_EXT_PINS 2 // External pins used for other stuff than valves
#define VALVE_EXT_PINS 30 // Number of valve pins

char serial_command_buffer_[128];
SerialCommands serial_commands_(&Serial, serial_command_buffer_, sizeof(serial_command_buffer_), "\r\n", " ");

void cmd_unrecognized(SerialCommands* sender, const char* cmd) {
	sender->GetSerial()->print("Unrecognized command [");
	sender->GetSerial()->print(cmd);
	sender->GetSerial()->println("]");
}

void cmd_scale(SerialCommands* sender) {
  sender->GetSerial()->print("SCALE ");
  sender->GetSerial()->println(scale.get_units(), 1);
}

void cmd_valve(SerialCommands* sender) {
  char* idStr = sender->Next();
	if (idStr == NULL) {
		sender->GetSerial()->println("ERROR NO_ID");
		return;
	}
  int valveId = atoi(idStr);

  if (valveId < 1 || valveId > VALVE_EXT_PINS) {
		sender->GetSerial()->println("ERROR NO_VALID_ID");
		return;
	}

  char* actionStr = sender->Next();
  if (actionStr == NULL) {
		sender->GetSerial()->println("ERROR NO_ACTION");
		return;
	}

  bool open = false;

  if (strcmp(actionStr, "OPEN") == 0) {
    open = true;
  } else if (strcmp(actionStr, "CLOSE") != 0) {
    sender->GetSerial()->println("ERROR NO_VALID_ACTION");
		return;  
  }

  byte extPin = valveId - 1  + RESERVED_EXT_PINS;

  expander[(int) (floor)(extPin / 16.f)].digitalWrite(extPin % 16, !open);

  sender->GetSerial()->print((open) ? "Opened" : "Closed");
  sender->GetSerial()->print(" valve ");
  sender->GetSerial()->print(valveId);
  sender->GetSerial()->print(" on expander ");
  sender->GetSerial()->print((int) floor(extPin / 16.f));
  sender->GetSerial()->print(" pin ");
  sender->GetSerial()->println(extPin % 16);
}

void cmd_pump(SerialCommands* sender) {
    char* actionStr = sender->Next();
  if (actionStr == NULL) {
		sender->GetSerial()->println("ERROR NO_ACTION");
		return;
	}

  if(strcmp(actionStr, "FORWARDS") == 0) {
    expander[0].digitalWrite(PUMP_FORWARDS_EXT_PIN, LOW);
    sender->GetSerial()->print("Started pump forwards");
  } else if(strcmp(actionStr, "BACKWARDS") == 0) {
    expander[0].digitalWrite(PUMP_BACKWARDS_EXT_PIN, LOW);
    sender->GetSerial()->print("Started pump backwards");
  } else if(strcmp(actionStr, "STOP") == 0){
    expander[0].digitalWrite(PUMP_BACKWARDS_EXT_PIN, HIGH);
    expander[0].digitalWrite(PUMP_FORWARDS_EXT_PIN, HIGH);
    sender->GetSerial()->println("Stopped pump");
  } else {
    sender->GetSerial()->println("ERROR NO_VALID_ACTION");
		return;  
  }
}

SerialCommand cmd_valve_("VALVE", cmd_valve);
SerialCommand cmd_scale_("SCALE", cmd_scale);
SerialCommand cmd_pump_("PUMP", cmd_pump);

void setup() {
  Serial.begin(BAUDRATE);
  Serial.println("COCKTAIL BOT 2 INTERFACE");

  //scale.begin(LOADCELL_DATA_PIN, LOADCELL_CLOCK_PIN);
  //scale.set_scale(1840.f);
  //scale.tare();

  for (byte i = 0; i < sizeof(expander); i++) {
    expander[i].begin_I2C(i);

    for (byte j = 0; j < 16; j++) {
      expander[i].pinMode(j, OUTPUT);
      expander[i].digitalWrite(j, HIGH);
    }
  }

  serial_commands_.SetDefaultHandler(cmd_unrecognized);
  serial_commands_.AddCommand(&cmd_valve_);
  serial_commands_.AddCommand(&cmd_scale_);
  serial_commands_.AddCommand(&cmd_pump_);
}

void loop() {
  serial_commands_.ReadSerial();
}